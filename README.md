# Chess Application

A learn-by-doing attempt at implementing a chess web application using microservices.
The service projects are:
1. [chess-app-frontend]
2. [chess-app-user]
3. [chess-app-gamehistory]
4. [chess-app-gamestate]
5. [chess-app-messaging]

This project contains scripts for local deployment of the application.
It requires a linux host with jdk8, docker, and node.js/npm.

## Local Deployment

Deploy:
```
./run deploy
```
This will clone the service projects into the [repos] directory and checkout the commits configured in [commits].
It will then deploy the services and attempt to open the frontend in browser using `xdg-open`.

Undeploy:
```
./run undeploy
```

## Local Development Deployment

Clone Repositories:
```
./run dev-repos
```
This will clone the service projects into [dev-repos].
It will NOT checkout the commits configured in [commits].
To develop a non-master branch, it will have to be checked out manually.
This command will fail if the [dev-repos] directory exists so that development work is not overwritten.

To deploy the services under development in [dev-repos]:
```
./run dev-deploy
```
After deploying, it will attempt to open the frontend in browser using `xdg-open`.

To redeploy after making changes:
```
./run dev-redeploy
```

To undeploy:
```
./run undeploy
```

When ready to update [commits] to the checked out commits in [dev-repos]:
```
./run dev-bump
```

## Service Documentation

Service documentation is served at `/chess-app-frontend/docs` via [swagger-ui].
Note that websocket endpoints are not currently documented.

## Verify

The deploy command generates sql for the service databases (see [generate-sql]).
Several users are created.
They all have `password` as their password.
The user with handle `bob` has the most initial data.

If chrome is installed on the host, then the application can be opened in a new browser session
(separate set of cookies from previously opened browser session) using
```
./run chrome-session
```
Then you can log in as a different user (user with handle `sally` for example) and play a game against the `bob` login.

## Notes

The [gamestate][chess-app-gamestate] and [messaging][chess-app-messaging] services are not currently horizontally scalable.
They use in-memory maps to connect websocket endpoints.
To make them horizontally scalable, a messaging service like kafka could be used.

Currently, the frontend defaults the pawn promotion piece type to queen without prompting the user to choose.

User registration has not been implemented yet.

[chess-app-frontend]: https://gitlab.com/akluball/chess-app-frontend
[chess-app-user]: https://gitlab.com/akluball/chess-app-user
[chess-app-gamehistory]: https://gitlab.com/akluball/chess-app-gamehistory
[chess-app-gamestate]: https://gitlab.com/akluball/chess-app-gamestate
[chess-app-messaging]: https://gitlab.com/akluball/chess-app-messaging

[repos]: repos
[dev-repos]: dev-repos
[commits]: commits
[generate-sql]: scripts/generate-sql

[swagger-ui]: https://swagger.io/tools/swagger-ui/