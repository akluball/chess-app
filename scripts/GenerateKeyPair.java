import java.security.KeyPairGenerator;
import java.security.KeyPair;
import java.util.Base64;

public class GenerateKeyPair {
    public static void main(String[] args) throws Exception {
        Base64.Encoder encoder = Base64.getEncoder();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        String privateKey = new String(encoder.encode(keyPair.getPrivate().getEncoded()));
        String publicKey = new String(encoder.encode(keyPair.getPublic().getEncoded()));
        System.out.println(String.format("%s %s", privateKey, publicKey));
    }
}