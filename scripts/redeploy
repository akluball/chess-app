#!/bin/sh

# Environment Expectations
# REPOS_DIR

dir="$(dirname "$(readlink -f "$0")")"

util() {
    "$dir/util/util" "$@"
}

. "$dir/config"
network="$NETWORK"
payara_container="$PAYARA_CONTAINER"

"$dir/generate-sql"

repos_dir="$REPOS_DIR"
build() {
    service_name="$1"
    local="$repos_dir/chess-app-$service_name"
    if (cd "$local" && ./gradlew --warn --warning-mode none war); then
        util print out "built $service_name service"
    else
        util print err "failed to build $service_name service"
    fi
}

build user
build messaging
build gamehistory
build gamestate

# frontend build requires out of network uris
payara_http_out_of_network="http://localhost:8080"
out_of_network_user_uri="$payara_http_out_of_network/chess-app-user"
out_of_network_messaging_uri="$payara_http_out_of_network/chess-app-messaging"
out_of_network_gamehistory_uri="$payara_http_out_of_network/chess-app-gamehistory"
out_of_network_gamestate_uri="$payara_http_out_of_network/chess-app-gamestate"

# frontend build environment
CHESSAPP_FRONTEND_DEPLOYMENTNAME=chess-app-frontend
CHESSAPP_USER_URI="$out_of_network_user_uri"
CHESSAPP_MESSAGING_URI="$out_of_network_messaging_uri"
CHESSAPP_GAMEHISTORY_URI="$out_of_network_gamehistory_uri"
CHESSAPP_GAMESTATE_URI="$out_of_network_gamestate_uri"
export CHESSAPP_FRONTEND_DEPLOYMENTNAME \
    CHESSAPP_USER_URI \
    CHESSAPP_MESSAGING_URI \
    CHESSAPP_GAMEHISTORY_URI \
    CHESSAPP_GAMESTATE_URI
build frontend

copy_to_container() {
    service_name="$1"
    war="$repos_dir/chess-app-$service_name/build/libs/chess-app-$service_name.war"
    docker cp "$war" "$payara_container:/chess-app-$service_name.war"
}

copy_to_container user
copy_to_container messaging
copy_to_container gamehistory
copy_to_container gamestate
copy_to_container frontend

redeploy() {
    service_name="$1"
    docker exec "$payara_container" \
        asadmin --user admin --passwordfile /opt/payara/passwordFile --interactive false \
        redeploy --name "chess-app-$service_name" "/chess-app-$service_name.war"
}

redeploy user
redeploy messaging
redeploy gamehistory
redeploy gamestate
redeploy frontend

xdg-open http://localhost:8080/chess-app-frontend
